class Settings:
    """ Una clase para guardar todas las configuraciones del juego """

    def __init__(self):
        """ Inicializamos la configuración del juego """
        #Configuraciones de la pantalla
        self.screen_width = 1200
        self.screen_height = 600
        self.ship_limit = 3
        self.bg_color = (230, 230, 230)

        # Definimos las configuraciones de las balas
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = (60, 60, 60)
        self.bullets_allowed = 3

        # Definimos las configuraciones de los aliens
        self.fleet_drop_speed = 10
        # Que tan rapido el juego se acelera
        self.speedup_scale = 1.1
        
        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        """ Inicializamos las configuraciones que cambiarán conforme avanza el juego """
        self.ship_speed = 1.5
        self.bullet_speed = 3.0
        self.alien_speed = 1.0
        
        # Dirección de la flota: 1 derecha, -1 izquierda
        self.fleet_direction = 1
    
    def increase_speed(self):
        self.ship_speed *= self.speedup_scale
        self.bullet_speed *= self.speedup_scale
        self.alien_speed *= self.speedup_scale

