import pygame

class Ship:
    """ Una clase para manejar la nave """
    
    def __init__(self, ai_game):
        """ Inicializamos la nave y su posición inicial """
        self.screen = ai_game.screen
        self.screen_rect = ai_game.screen.get_rect()
        self.settings = ai_game.settings

        # Cargamos la imagen de la nave y su rect
        self.image = pygame.image.load("images/ship.bmp")
        self.rect = self.image.get_rect()

        # Cargamos cada nueva nave en el centro de la parte inferior de la pantalla
        self.rect.midbottom = self.screen_rect.midbottom

        # Ponemos la posición de x
        self.x = float(self.rect.x)

        # Creamos la bandera de la nave hacia la derecha.
        self.moving_right = False
        # Creamos la bandera de la nave hacia la izquierda.
        self.moving_left = False

    def blitme(self):
        """ Dibujamos la nave en su posición actual """

        self.screen.blit(self.image, self.rect)
    
    def update(self):
        """ Actualizamos la posición de la nave basados en el movimiento de la bandera """
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.x += self.settings.ship_speed
        elif self.moving_left and self.rect.left > 0:
            self.x -= self.settings.ship_speed

        self.rect.x = self.x

    def center_ship(self):
        self.rect.midbottom = self.screen_rect.midbottom
        self.x = float(self.rect.x)