class GameStats:
    """ Trackeamos las estadisticas de Alien Invasion """

    def __init__(self, ai_game):
        """ Inicializamos las estadisticas """

        self.settings = ai_game.settings
        self.reset_stats()

        # Hacemos que el juego empiece en estado inactivo
        self.game_active = False

    def reset_stats(self):
        """ Inicializamos las estadisticas que pueden cambiar durante el juego """
        self.ship_left = self.settings.ship_limit