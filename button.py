import pygame
import pygame.font

class Button:

    def __init__(self, ai_game, msg):
        """ Iniciamos los atributos del boton """
        self.screen  = ai_game.screen
        self.screen_rect = self.screen.get_rect()

        # Seteamos las dimensiones y propiedades del boton
        self.width, self.height = 200, 50
        self.button_color = (0, 255, 0)
        self.text_color = (255, 255, 255)
        self.font = pygame.font.SysFont(None, 48)

        # Construimos la recta del boton y lo centramos
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        self.rect.center = self.screen_rect.center


        # Configuramos el mensaje del boton
        self._prep_msg(msg)
    
    def _prep_msg(self, msg):
        """ Convertimos el texto en una imagen y lo ponemos en el centro del boton """
        self.msg_image = self.font.render(msg, True, self.text_color, self.button_color)
        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.rect.center
    
    def draw_button(self):
        # Pintamos el boton y luego el texto
        self.screen.fill(self.button_color, self.rect)
        self.screen.blit(self.msg_image, self.msg_image_rect)