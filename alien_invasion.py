import sys
from time import sleep
import pygame

from settings import Settings
from ship import Ship
from bullet import Bullet
from alien import Alien
from game_stats import GameStats
from button import Button

class AlienInvasion:
    """ Clase para manejar todos los archivos y el comportamiento """

    def __init__(self):
        """ Inicializamos el juego y creamos los recursos del juego """
        pygame.init()
        self.settings = Settings()

        # self.screen = pygame.display.set_mode((self.settings.screen_width,self.settings.screen_height))
        self.screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        self.settings.screen_width = self.screen.get_rect().width
        self.settings.screen_height = self.screen.get_rect().height
        pygame.display.set_caption("Alien invasion")

        # Creamos una instancia para guardar las estadísticas del juego
        self.stats = GameStats(self)

        self.ship = Ship(self)

        self.bullets = pygame.sprite.Group()
        self.aliens = pygame.sprite.Group()
        
        self._create_fleet()


        # Creamos el boton de play
        self.play_button = Button(self, "Jugar")

    def run_game(self):
        """ Empezamos el loop principal del juego """
        while True:
            self._check_events()
            if self.stats.game_active:
                self.ship.update()
                self._update_bullets()
                self._update_aliens()
            
            self._update_screen()

    def _check_events(self):
        """ Para checar eventos del mouse y el teclado """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup(event)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                self._check_play_button(mouse_pos)

    def _check_play_button(self, mouse_pos):
        """ Empezamos un juego nuevo cuando el jugador apreta play """
        button_clicked = self.play_button.rect.collidepoint(mouse_pos)
        if button_clicked and not self.stats.game_active:
            # Reiniciamos las configuraciones del juego
            self.settings.initialize_dynamic_settings()

            self.stats.reset_stats()
            self.stats.game_active = True

            # Eliminamos todas las naves restantes y las balas
            self.aliens.empty()
            self.bullets.empty()

            # Creamos una nueva flota y centramos la nave
            self._create_fleet()
            self.ship.center_ship()

            # Ocultamos el bototn de Play
            pygame.mouse.set_visible(False)

    def _update_bullets(self):
        """
            Actualizamos la posición de las balas y nos deshacemos de las balas que ya
            no estan en la pantalla.
        """

        self.bullets.update()

        # Nos deshacemos de las balas que desaparecen
        for bullet in self.bullets.copy():
            if bullet.rect.bottom <= 0:
                self.bullets.remove(bullet)

        self._check_bullet_alien_collition()

    def _check_bullet_alien_collition(self):
        """ Manejo de la interacción entre los aliens y las balas """

        # Checamos si alguna bala toco con un alien para entonces desaparecerlos
        pygame.sprite.groupcollide(self.bullets, self.aliens, True, True)

        # Checamos si ya no existen aliens, en cuyo caso creamos una nueva flota
        if not self.aliens:
            self.bullets.empty()
            self._create_fleet()
            self.settings.increase_speed()

    def _check_keydown(self, event):
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = True
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = True
        elif event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()
    
    def _check_keyup(self, event):
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = False

    def _update_screen(self):
        """ Actualizamos las imágenes de la pantalla """
        # Volvemos a pintar toda la pantalla después de cada pasada del loop
        self.screen.fill(self.settings.bg_color)
        self.ship.blitme()
        for bullet in self.bullets.sprites():
            bullet.draw_bullet()
        
        self.aliens.draw(self.screen)

        # Dibujamos el boton de play si el juego esta inactivo
        if not self.stats.game_active:
            self.play_button.draw_button()

        # Hacemos que el dibujo más reciente se pinte sobre la pantalla.
        pygame.display.flip()

    def _fire_bullet(self):
        """ Creamos una nueva bala y la agregamos al grupo """
        if len(self.bullets) < self.settings.bullets_allowed:
            new_bullet = Bullet(self)
            self.bullets.add(new_bullet)

    def _create_fleet(self):
        """ Creamos una flota de naves """
        # Creamos una nave
        alien = Alien(self)

        # Calculamos el número de aliens que caben en una linea
        # El espacio entre aliens es igual al tamaño de un alien
        alien_width, alien_height = alien.rect.size
        available_space_x = self.settings.screen_width - (2 * alien_width)
        number_aliens_x = available_space_x // (2 * alien_width)

        # Obtenemos la cantidad de columnas que se pueden poner aliens
        ship_height = self.ship.rect.height
        available_space_y = (self.settings.screen_height - (3 * alien_height) - ship_height)
        number_rows = available_space_y // (2 * alien_height)


        # Creamos las columnas de aliens
        for row_number in range(number_rows):
            # Creamos la primera fila de aliens
            for alien_number in range(number_aliens_x):
                # Creamos un alien y lo ponemos en fila
                self._create_alien(alien_number, row_number)
    
    def _create_alien(self, alien_number, row_number):
        alien = Alien(self)
        alien_width, alien_height = alien.rect.size
        alien.x = alien_width + 2 * alien_width * alien_number
        alien.rect.x = alien.x
        alien.rect.y = alien.rect.height + 2 * alien.rect.height * row_number
        self.aliens.add(alien)

    def _update_aliens(self):
        """ 
            Actualizamos la posición de los aliens.
            Si tienen todavia espacio que se sigan moviendo, si no los cambiamos de ruta.
        """
        self._check_fleet_edges()
        self.aliens.update()

        # Vemos las colisiones entre los aliens y la nave
        if pygame.sprite.spritecollideany(self.ship, self.aliens):
            self._ship_hit()

        # Buscamos los aliens que tocan la parte inferior de la pantalla 
        self._check_aliens_bottom()

    def _check_fleet_edges(self):
        """ Manejo para cuando algún alien de la flota toca los bordes """
        for alien in self.aliens.sprites():
            if alien.check_edges():
                self._change_fleet_direction()
                break
    
    def _change_fleet_direction(self):
        """ Cambiamos el rumbo de toda la flota """
        for alien in self.aliens.sprites():
            alien.rect.y += self.settings.fleet_drop_speed
        self.settings.fleet_direction *= -1

    def _ship_hit(self):
        """ Respondemos cuando la nave es golpeada por un alien """

        if self.stats.ship_left > 0:
            # Bajamos la cantidad de vidas que quedan
            self.stats.ship_left -= 1

            # Nos deshacemos de todos los aliens y balas restantes
            self.aliens.empty()
            self.bullets.empty()

            # Creamos una nueva flota y centramos la nave
            self._create_fleet()
            self.ship.center_ship()

            # Pausamos el juego
            sleep(0.5)
        else:
            self.stats.game_active = False
            pygame.mouse.set_visible(True)

    def _check_aliens_bottom(self):
        """ Checamos si algun alien toco el fondo de la pantalla """ 

        screen_rect = self.screen.get_rect()
        for alien in self.aliens.sprites():
            if alien.rect.bottom >= screen_rect.bottom:
               # Tratamos esto como si fuera la nave golpeada 
               self._ship_hit()
               break

if __name__ == "__main__":
    # Hacemos una instancia del juego y lo corremos
    ai = AlienInvasion()
    ai.run_game()

